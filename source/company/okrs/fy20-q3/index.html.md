---
layout: markdown_page
title: "FY20-Q3 OKRs"
---

This fiscal quarter will run from August 1, 2019 to October 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV.

1. VP Engineering: [Enterprise-level availability](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4893)
    1. Development: [Improve Predictability](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4841)

### 2. CEO: Popular next generation product.

1. VP Product: Establish a new [Problem Validation](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation) process. Complete at least 15 problem validation cycles (one per PM, excluding Growth). Complete at least 5 qualitative customer interviews per PM.
1. VP Product: Improve the quality of product usage data, to enable better data driven product decisions. Finalize top Product and Growth [KPI definitions](/handbook/product/metrics/), including [SMAU](/handbook/product/metrics/#adoption) definitions for each stage.  Get reliable and accurate product usage data from both Self Hosted + SaaS customer environments. Deliver SMAU dashboards with actionable data for each stage.
1. VP Product: Communicate 1 year product Plans at the Section level to inform product investment decisions. Deliver 1 year plan content for Dev, Ops, and CI/CD sections by end of Q3. Stretch goal for Growth, Enablement, Secure, and Defend (pending Director hires).
1. VP of Product Strategy: Get strategic thinking into the org. 3 [strategy reviews](https://gitlab.com/gitlab-com/Product/issues/379) for key industry categories (project management, application security testing, CD/RA).
1. VP Engineering: [Dogfood everything](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4894)
    1. Development: [Increase MRs, Maintain MR to Author ratio, first response KPI](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4837)
    1. UX: Make GitLab the most usable DevOps platform on the market. Raise our [SUS score](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability) to *76.2* from 74.4 in Q1 and 73.5 in Q2. 
    1. UX: Improve the quality and consistency of our UI. Turn remaining ## foundational UI components into single-source-of-truth components that are implemented in Vue and thoroughly documented on design.gitlab.com.

### 3. CEO: Great team.

1. VP Product:  Hire at least 18 PM roles in Q3 to catch up with annual hiring plan.
1. VP Engineering: [Scale gracefully to 500+ members](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4895)
    1. Development: [Hire to Plan, New Manager Documenation, Increase Maintainers](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4838)
